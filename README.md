# Gitlab Runner Generator

## Description

The "Gitlab Runner Generator" is used to create a GitLab Runner that is configured to run k8s operator test based on Ansible molecule test framework. Docker is pre-installed. 

## Manual setup to be automated

1. Set GitLab `registration_token` in `examples/runner-docker/terraform.tfvars`
2. Execute the Terraform script from folder `examples/runner-docker`
3. `ssh` in the generated EC2 instance using `examples/runner-docker/generated/id_rsa`
4. Add kubectl config in user `gitlab-runner`'s `~/.kube/config`, since the current kubectl config that is automatically passed by GitLab doesn't work.
5. Executes:
   
```
sudo su
export ARCH=$(case $(arch) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(arch) ;; esac)
export OS=$(uname | awk '{print tolower($0)}')
export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/latest/download
curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}
gpg --keyserver keyserver.ubuntu.com --recv-keys 052996E2A20B5C7E
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt
curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt.asc
gpg -u "Operator SDK (release) <cncf-operator-sdk@cncf.io>" --verify checksums.txt.asc
chmod +x operator-sdk_${OS}_${ARCH} &&  mv operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk
curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
cp kustomize /bin
amazon-linux-extras install python3.8 -y  # yum install python36 -y
pip3 install ansible molecule docker yamllint openshift==0.11 kubernetes==11.0.0
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(<kubectl.sha256) kubectl" | sha256sum --check
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.10.0/kind-linux-amd64
chmod +x ./kind
mv ./kind /bin/kind
usermod -aG docker gitlab-runner
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
sysctl --system
wget https://github.com/mikefarah/yq/releases/download/v4.2.0/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
exit
ansible --version
molecule --version
kubectl version
kustomize version
operator-sdk version
kind --version
ansible-galaxy --version
```

## Give it a try

- See this [branch](https://gitlab.com/darkwolf-cc2/start-here/-/blob/add-molecule-in-build/.gitlab-ci.yml) in action.